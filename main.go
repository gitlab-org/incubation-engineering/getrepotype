package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/gitlab-org/getrepotype/app"
	"log"
	"os"
)

func main() {
	path, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	fingerprints := app.MatchFingerprints(path)

	result, err := json.Marshal(fingerprints)

	fmt.Printf("%s\n", string(result))
}
