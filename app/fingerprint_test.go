package app

import (
	"github.com/google/go-cmp/cmp"
	"testing"
)

func TestNewFromTemplate(t *testing.T) {
	tpl := []byte("name: testcase1\nexpect:\n  file: ./package.json\n  exists: true\n")
	want := Fingerprint{
		Name: "testcase1",
		Expect: Rule{
			 File: "./package.json" ,
			 Exists: true,
		},
	}

	fingerprint, err := NewFromTemplate(tpl)

	if err != nil {
		t.Fatal("Template Parsing causes an unexpected error")
	}

	if !cmp.Equal(want, fingerprint) {
		t.Fatalf("Fingerprint was not parsed as expected. Expected: %#v, Actual: %#v", want, fingerprint)
	}
}

func TestFingerprintMatches(t *testing.T) {
	fingerprint := Fingerprint{
		Name:   "testcase1",
		Expect: Rule{ File: "./package.json" },
	}

	matches, err := fingerprint.Matches("../testcases/testcase1case1")

	if err != nil {
		t.Fatalf("fingerprint.AppliesTo caused an unexpected error: %v", err)
	}

	if matches != true {
		t.Fatal("fingerprint.AppliesTo should return true")
	}
}