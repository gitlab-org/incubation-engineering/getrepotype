package app

import (
	"encoding/json"
	"gopkg.in/yaml.v2"
)

type Fingerprint struct {
	Name     string `json:"name"`
	Homepage string `json:"homepage,omitempty"`
	Expect   Rule   `json:"-"`
}

func NewFromTemplate(template []byte) (Fingerprint, error) {
	var fingerprint Fingerprint
	err := yaml.Unmarshal(template, &fingerprint)
	return fingerprint, err
}

func (f *Fingerprint) Matches(path string) (bool, error) {
	return f.Expect.AppliesTo(path)
}

func (f *Fingerprint) GetAsJson() ([]byte, error) {
	return json.Marshal(f)
}
