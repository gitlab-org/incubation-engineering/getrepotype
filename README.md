# getrepotype

This is a CLI tool that scans your working directory to determine the type of application: For example, whether a
repo contains the sourcecode for an iOS app, a Ruby on Rails app, a Next.js app, a Docker image &mdash; or a combination
of those.

Assuming you run `getrepotype` in a working directory that contains some Ruby on Rails code as well as a Dockerfile
that can be used to create a Docker image with your app, this tool will return a list of all applicable app types:

- `Ruby on Rails`
- `Docker image`

## How it works

This app is intentionally pretty dumb. It is based on pretty simple rulesets, called "Fingerprints". Most [Fingerprints
should come bundled with this app](https://gitlab.com/gitlab-org/incubation-engineering/getrepotype/-/tree/main/app/fingerprints), but a future version will allow mounting an external rule repositiory.

### Fingerprints

These [fingerprints](https://gitlab.com/gitlab-org/incubation-engineering/getrepotype/-/tree/main/app/fingerprints) are the core to the app. They specify patterns `getrepotype` will check for, for example whether a certain file or glob pattern exists (or does not exist) in the tested repo.

This is what a fingerprint could look like:

```yaml
name: nuxt.js
homepage: https://nuxtjs.org

expect:
  file: nuxt.config.js
  exists: true
  any_of:
    - file: nuxt.config.js
    - file: package.json
      pattern: '^"nuxt": ".*",?$'
  all_of:
    - file: package.json
      exists: true
  none_of:
    - any_of:
        - file: Dockerfile
        - file: docker-compose.yml
```

This is obviously an unrealistic example, but it shows the capabilities of the `fingerprint.yml`.

Let's break it down.

#### The header

The header of the fingerprint shows some metadata about the application type:

- **`name`**: The unique name of the application type.

  This can be namespaced, eg. `nuxt.js/nuxt-3`

- **`homepage`** (optional): A homepage of the application type, for references, docs, etc.

#### The Ruleset

The third field of the `fingerprint.yaml` is the `expect` property. Here we need to define a **Rule** that uniquely
identifies the application type.

```yaml
# fingerprint.yml
expect: <Rule>
```

Most Frameworks have a certain unique configuration file, e.g. `nuxt.config.js` in our example. We can utilize **Rule**s
to check for their presence.

A **Rule** is a type of object that has the following properties:

- **`file`**: A file path or Glob pattern relative to the working directory. This is optional, if there are sub-rules
  specified with `any_of`, `all_of` or `none_of` (see below)
- **`exists`**: Set to `true` if we should expect this file to exist. Set to `false` if this file _must not_ exist.
- **`pattern`** (optional): A Regex pattern to check the file against. The rule will only apply if
  the pattern matches.

Rules can be nested. To do so, you can specify the following fields inside a Rule:

- **`all_of`**: A list of other **Rules** that all need to apply
- **`any_of`**: A list of other **Rules** of which at least one needs to apply
- **`none_of`**: A list of other **Rules** all of which must not apply

## Roadmap

- Support external Fingerprint Repositories
- Parallelize rule check
- Allow fingerprints to `extend` other fingerprints, eg. only test a fingerprint if another fingerprint applies.
