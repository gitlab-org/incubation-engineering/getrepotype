package app

import (
	"os"
	"path/filepath"
	"regexp"
	"sync"
)

type Rule struct {
	File    string
	Exists  bool
	Pattern string
	AllOf   []Rule `yaml:"all_of"`
	AnyOf   []Rule `yaml:"any_of"`
	NoneOf  []Rule `yaml:"none_of"`
}

func (c *Rule) matchesTheAllOfRule(path string) (bool, error) {
	if c.AllOf == nil {
		return true, nil
	}
	for _, subRule := range c.AllOf {
		matches, err := subRule.AppliesTo(path)

		if err != nil {
			return false, err
		}

		if !matches {
			return false, nil
		}
	}
	return true, nil
}

func (c *Rule) matchesTheAnyOfRule(path string) (bool, error) {
	if c.AnyOf == nil {
		return true, nil
	}
	for _, subRule := range c.AnyOf {
		matches, err := subRule.AppliesTo(path)

		if err != nil {
			return false, err
		}

		if matches {
			return true, nil
		}
	}
	return false, nil
}

func (c *Rule) matchesTheNoneOfRule(path string) (bool, error) {
	if c.NoneOf == nil {
		return true, nil
	}
	for _, subRule := range c.NoneOf {
		matches, err := subRule.AppliesTo(path)

		if err != nil {
			return false, err
		}

		if matches {
			return false, nil
		}
	}
	return true, nil
}

func (c *Rule) doesFileMatchThePattern(filepath string) (bool, error) {
	contents, err := os.ReadFile(filepath)

	if err != nil {
		return false, err
	}

	r, err := regexp.Compile(c.Pattern)

	if err != nil {
		return false, err
	}

	return r.MatchString(string(contents)), nil
}

func (c *Rule) doesAnyFileMatchThePattern(matchedFileNames []string) (bool, error) {
	if c.Pattern == "" {
		return true, nil
	}

	var anyMatchesRegex bool

	for _, file := range matchedFileNames {
		doesMatchRegex, err := c.doesFileMatchThePattern(file)

		if err != nil {
			return false, err
		}

		if doesMatchRegex {
			anyMatchesRegex = true
			break
		}
	}

	return anyMatchesRegex, nil
}

func (c *Rule) AppliesTo(path string) (bool, error) {

	glob := filepath.Join(path, c.File)
	wg := sync.WaitGroup{}

	if c.File != "" {
		matches, err := filepath.Glob(glob)

		if err != nil {
			return false, err
		}

		if len(matches) <= 0 {
			return false, nil
		}

		matchesThePattern, err := c.doesAnyFileMatchThePattern(matches)

		if err != nil {
			return false, err
		}

		if !matchesThePattern {
			return false, nil
		}
	}

	wg.Add(3)
	errors := make(chan error)
	var matchesTheAllOfRule bool
	var matchesTheAnyOfRule bool
	var matchesTheNoneOfRule bool

	go func() {
		var err error
		matchesTheAllOfRule, err = c.matchesTheAllOfRule(path)

		if err != nil {
			errors <- err
		}
		wg.Done()
	}()

	go func() {
		var err error

		matchesTheAnyOfRule, err = c.matchesTheAnyOfRule(path)

		if err != nil {
			errors <- err
		}
		wg.Done()
	}()

	go func() {
		var err error

		matchesTheNoneOfRule, err = c.matchesTheAnyOfRule(path)

		if err != nil {
			errors <- err
		}
		wg.Done()
	}()

	// TODO: check error channel

	wg.Wait()
	close(errors)

	return matchesTheAllOfRule && matchesTheAnyOfRule && matchesTheNoneOfRule, nil
}
