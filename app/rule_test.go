package app

import (
	"testing"
)

func TestMatches(t *testing.T) {
	rule := Rule{ File: "./package.json" }

	matches, err := rule.AppliesTo("../testcases/case1")

	if err != nil {
		t.Fatalf("fingerprint.AppliesTo caused an unexpected error: %v", err)
	}

	if matches != true {
		t.Fatal("fingerprint.AppliesTo should return true")
	}
}

func testRule(t *testing.T, rule *Rule, shouldMatch bool) {
	matches, err := rule.AppliesTo("../testcases/case1")

	if err != nil {
		t.Fatalf("fingerprint.AppliesTo caused an unexpected error: %v", err)
	}

	if matches != shouldMatch {
		t.Fatalf("fingerprint.AppliesTo should return %v", shouldMatch)
	}
}

func TestCumulatedCharacteristicsFailing(t *testing.T) {
	testRule(t,
		&Rule{
		AllOf: []Rule{
			{File: "./package.json"},
			{File: "./doesnt_exist.txt"},
		},
	},
		false,
	)
}

func TestCumulatedCharacteristicsSucceeding(t *testing.T) {
	testRule(t,
		&Rule{
			AllOf: []Rule{
				{File: "./package.json"},
				{File: "./some_other_file.txt"},
			},
		},
		true,
	)
}


func TestAlternateCharacteristicsSucceeding(t *testing.T) {
	testRule(t,
		& Rule{
			AnyOf: []Rule{
				{File: "./package.json"},
				{File: "./doesnt_exist.txt"},
			},
		},
		true,
	)
}

func TestAlternateCharacteristicsFailing(t *testing.T) {
	testRule(t,
		&Rule{
			AnyOf: []Rule{
				{File: "./doesnt_exist.json"},
				{File: "./doesnt_exist.txt"},
			},
		},
		false,
	)
}