module gitlab.com/gitlab-org/getrepotype

go 1.17

require (
	gopkg.in/yaml.v2 v2.4.0
    github.com/google/go-cmp v0.5.7 // indirect
)
