package app

import (
	"embed"
	"log"
	"path"
)

//go:embed fingerprints/*
var fingerprints embed.FS

var rootDir = "fingerprints"

func readFingerprints(fingerprintsDir string, loaded chan []byte) {
	defer close(loaded)

	directory := rootDir
	if fingerprintsDir != "" {
		directory = fingerprintsDir
	}

	dirEntries, err := fingerprints.ReadDir(directory)
	if err != nil {
		log.Fatal("Unable to read builtin fingerprint repository")
	}
	for _, entry := range dirEntries {
		if entry.IsDir() {
			// TODO: Make this function recursive to support nested folders
			continue
		}
		file, _ := fingerprints.ReadFile(path.Join(directory, entry.Name()))
		if err != nil {
			log.Printf("Unable to open Fingerprint File \"%s\" from builtin fingerprint repository", entry.Name())
			return
		}
		loaded <- file
	}
}

func loadFingerprints(fingerprints chan Fingerprint) {
	defer close(fingerprints)

	fileContents := make(chan []byte, 100)

	go readFingerprints("", fileContents)

	for file := range fileContents {
		fingerprint, err := NewFromTemplate(file)
		if err != nil {
			log.Printf("Unable to parse fingerprint file. File content:\n%s", file)
			continue
		}
		fingerprints <- fingerprint
	}
}

func MatchFingerprints(repoDir string) []Fingerprint {
	fingerprints := make(chan Fingerprint, 100)
	matches := make([]Fingerprint, 0, 1000)

	go loadFingerprints(fingerprints)

	for fp := range fingerprints {
		doesMatch, err := fp.Matches(repoDir)
		if err != nil {
			log.Printf("Error trying to match fingerprint: \"%s\"\n%v\\n", fp.Name, err)
			continue
		}
		if doesMatch {
			matches = append(matches, fp)
		}
 	}
	return matches
}